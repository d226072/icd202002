# Projeto: Analise da correlatividade de variáveis meteorológicas com novos casos de Covid-19 nos estados do Brasil

## Equipe: Trio Nordeste

## Descrição: 

Com o impacto mundial que a Covid-19 tem causado, em número de infecções e mortes, se faz necessário investigar quais os fatores que corroboram para a disseminação do vírus desta doença, relacionando fatores climáticos com algumas outras variáveis.
Uma das propostas estudadas para tentar modelar as causas da grande quantidade de casos da doença é a relação de influência da temperatura no espalhamento do vírus, seja através do ar ou de superfícies.
Também observou-se em outros trabalhos a relação com outras variáveis meteorológicas, sendo elas precipitação total, temperatura do ar, umidade relativa do ar, velocidade do vento, radiação global e pressão atmosférica.
Contudo, grande parte destes estudos foram conduzidos em países como China, Irã e Itália, ou de modo global sem se atentar as particularidades de cada local.
Dentre os trabalhos investigados, alguns indicam que com o aumento da temperatura é observada uma redução no número de novos casos.
Este trabalho tem por objetivo investigar a existência ou não de correlatividade entre novos casos de Covid-19 e variáveis meteorológicas nos estados Brasileiros.


## Membros senior:

* Andressa, RA262878, @andressamarcal, a262878@dac.unicamp.br, Mestrado Ciência da Computação, Unicamp
* Décio, RA226072, @d226072, d226072@dac.unicamp.br, Doutorado Ciência da Computação, Unicamp
* Diego, RA230640, @alyssonbm, d230640@dac.unicamp.br, Doutorado Ciência da Computação, Unicamp.
